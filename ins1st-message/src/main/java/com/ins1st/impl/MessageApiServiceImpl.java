package com.ins1st.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.ins1st.api.MessageApi;
import com.ins1st.entity.JavaMail;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-27 17:36
 **/
@RestController
public class MessageApiServiceImpl implements MessageApi {

    private static final Logger log = LoggerFactory.getLogger(MessageApiServiceImpl.class);

    @Value("${email.host-name}")
    private String hostName;
    @Value("${email.send}")
    private String send;
    @Value("${email.password}")
    private String password;

    @Value("${aliyun.sms.access_key}")
    private String accessKey;
    @Value("${aliyun.sms.access_secret}")
    private String accessSecret;
    @Value("${aliyun.sms.domain}")
    private String domain;
    @Value("${aliyun.sms.version}")
    private Date version;
    @Value("${aliyun.sms.action}")
    private String action;
    @Value("${aliyun.sms.sign_name}")
    private String signName;



    @Override
    public boolean sendEmail(JavaMail javaMail) {
        HtmlEmail email = new HtmlEmail();
        try {
            email.setHostName(hostName);
            email.setCharset("utf-8");
            email.setFrom(javaMail.getTo(), javaMail.getName());
            email.setAuthentication(send, password);
            email.setSubject(javaMail.getSubject());
            email.setMsg(javaMail.getMsg());
            email.setSSLOnConnect(true);
            email.send();
            return true;
        } catch (EmailException e) {
            e.printStackTrace();
            log.info("邮件发送失败,to: {}", javaMail.getTo());
            return false;
        }
    }

    @Override
    public boolean sendSms(String tpId, String mobile, Map<String, String> map) {
        DefaultProfile profile = DefaultProfile.getProfile("default", accessKey, accessSecret);
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain(domain);
        request.setVersion(new SimpleDateFormat("yyyy-MM-dd").format(version));
        request.setAction(action);
        request.putQueryParameter("PhoneNumbers", mobile);
        request.putQueryParameter("TemplateParam", JSON.toJSONString(map));
        request.putQueryParameter("SignName", signName);
        request.putQueryParameter("TemplateCode", tpId);
        try {
            CommonResponse response = client.getCommonResponse(request);
            JSONObject jsonObject = JSONObject.parseObject(response.getData());
            String message = jsonObject.getString("Message");
            if (StringUtils.equals(message, "OK")) {
                log.info("mobile : {} 短信发送成功", mobile);
                return true;
            } else {
                log.error("短信发送失败,原因:{}" + message);
                return false;
            }
        } catch (ClientException e) {
            e.printStackTrace();
            log.error("短信发送失败");
            return false;
        }
    }
}
