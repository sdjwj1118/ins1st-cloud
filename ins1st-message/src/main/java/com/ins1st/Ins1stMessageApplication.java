package com.ins1st;

import com.ins1st.plugins.MybatisInterceptor;
import com.ins1st.util.DatabaseUtil;
import com.ins1st.util.SqlRunUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
@EnableFeignClients
@ComponentScan(value = "com.ins1st", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE
		, classes = {DatabaseUtil.class, SqlRunUtil.class, MybatisInterceptor.class}))
public class Ins1stMessageApplication {

	private static final Logger log = LoggerFactory.getLogger(Ins1stMessageApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(Ins1stMessageApplication.class, args);
		log.info(Ins1stMessageApplication.class.getSimpleName() + " is start success");
	}

}
