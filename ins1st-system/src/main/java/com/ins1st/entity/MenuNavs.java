package com.ins1st.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-11 10:38
 **/
public class MenuNavs implements Serializable {

    private String title;
    private String href;
    private String fontFamily = "ok-icon";
    private String icon;
    private boolean spread = false;
    private boolean isCheck = false;
    private List<MenuNavs> children;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isSpread() {
        return spread;
    }

    public void setSpread(boolean spread) {
        this.spread = spread;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public List<MenuNavs> getChildren() {
        return children;
    }

    public void setChildren(List<MenuNavs> children) {
        this.children = children;
    }
}
