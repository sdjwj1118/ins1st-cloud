package com.ins1st.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ins1st.entity.SysServiceLog;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 16:16
 **/
public interface SysServiceLogMapper extends BaseMapper<SysServiceLog> {
}
