package com.ins1st.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ins1st.entity.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}
