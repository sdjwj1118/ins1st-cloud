package com.ins1st.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ins1st.api.SysTenantsApi;
import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysTenants;
import com.ins1st.mapper.SysTenantsMapper;
import com.ins1st.util.SqlRunUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-22 14:42
 **/
@RestController
@Transactional
public class SysTenantsApiImpl extends ServiceImpl<SysTenantsMapper, SysTenants> implements SysTenantsApi {

    @Autowired
    private SqlRunUtil sqlRunUtil;

    @Override
    public R<List<SysTenants>> queryList(SysTenants sysTenants) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(StringUtils.isNotBlank(sysTenants.getStatus()), "status", sysTenants.getStatus());
        queryWrapper.eq(StringUtils.isNotBlank(sysTenants.getTenantName()), "tenant_name", sysTenants.getTenantName());
        R<List<SysTenants>> r = new R<>();
        List<SysTenants> sysTenantsList = this.list();
        r.setData(sysTenantsList);
        return r;
    }

    @Override
    public PageResult page(SysTenants sysTenants) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(StringUtils.isNotBlank(sysTenants.getStatus()), "status", sysTenants.getStatus());
        queryWrapper.eq(StringUtils.isNotBlank(sysTenants.getTenantName()), "tenant_name", sysTenants.getTenantName());
        IPage<SysTenants> page = this.baseMapper.selectPage(new Page(sysTenants.getPage(), sysTenants.getLimit()), queryWrapper);
        return R.page(page);
    }

    @Override
    public R insertOrUpdate(SysTenants sysTenants) {
        if (StringUtils.isNotBlank(sysTenants.getId())) {
            this.baseMapper.updateById(sysTenants);
        } else {
            String dbName = IdWorker.getIdStr();
            sysTenants.setCreateTime(new Date());
            sysTenants.setTenantId(dbName);
            this.baseMapper.insert(sysTenants);
            sqlRunUtil.createDatabase(dbName);
            sqlRunUtil.runClassPathSql("sql/tenant.sql", dbName);
        }
        return R.success("操作成功");
    }

    @Override
    public R remove(String id) {
        this.baseMapper.deleteById(id);
        return R.success("操作成功");
    }

    @Override
    public R<SysTenants> selectOne(SysTenants sysTenants) {
        QueryWrapper<SysTenants> qw = new QueryWrapper<>();
        qw.eq("id", sysTenants.getId());
        SysTenants result = this.baseMapper.selectOne(qw);
        return R.success(result);
    }

}
