package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysTenants;
import com.ins1st.entity.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-22 14:41
 **/
@FeignClient(name = "ins1st-system", contextId = "SysTenantsApi")
public interface SysTenantsApi {


    @RequestMapping(value = "/tenant/queryList", method = RequestMethod.POST)
    R<List<SysTenants>> queryList(@RequestBody SysTenants sysTenants);

    @RequestMapping(value = "/tenant/page", method = RequestMethod.POST)
    PageResult page(@RequestBody SysTenants sysTenants);

    @RequestMapping(value = "/tenant/insertOrUpdate", method = RequestMethod.POST)
    R insertOrUpdate(@RequestBody SysTenants sysTenants);

    @RequestMapping(value = "/tenant/remove", method = RequestMethod.POST)
    R remove(@RequestParam("id") String id);

    @RequestMapping(value = "/tenant/selectOne", method = RequestMethod.POST)
    R<SysTenants> selectOne(@RequestBody SysTenants sysTenants);
}
