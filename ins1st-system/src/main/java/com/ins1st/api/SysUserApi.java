package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "ins1st-system", contextId = "SysUserApi")
public interface SysUserApi {


    @RequestMapping(value = "/user/page", method = RequestMethod.POST)
    PageResult page(@RequestBody SysUser sysUser);

    @RequestMapping(value = "/user/insertOrUpdate", method = RequestMethod.POST)
    R insertOrUpdate(@RequestBody SysUser sysUser);

    @RequestMapping(value = "/user/remove", method = RequestMethod.POST)
    R remove(@RequestParam("id") String id);

    @RequestMapping(value = "/user/selectOne", method = RequestMethod.POST)
    R<SysUser> selectOne(@RequestBody SysUser sysUser);
}
