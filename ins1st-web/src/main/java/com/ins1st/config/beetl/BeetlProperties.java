package com.ins1st.config.beetl;

import java.util.Properties;

/**
 * @program: ins1st-plus
 * @description: beetl配置类
 * @author: coderSun
 * @create: 2019-09-02 21:26
 **/
public class BeetlProperties {


    /**
     * 配置beetl属性
     * @return
     */
    public Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("DELIMITER_STATEMENT_START", "@");
        properties.setProperty("DELIMITER_STATEMENT_END", "null");
        properties.setProperty("RESOURCE.tagRoot", "common/tags");
        properties.setProperty("RESOURCE.tagSuffix", "tag");
        properties.setProperty("RESOURCE.autoCheck", "true");
        return properties;
    }


}
