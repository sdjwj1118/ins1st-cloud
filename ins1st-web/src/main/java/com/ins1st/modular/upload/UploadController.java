package com.ins1st.modular.upload;

import com.ins1st.base.R;
import com.ins1st.entity.SysUpload;
import com.ins1st.util.QiNuiUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-22 11:38
 **/
@RestController
@RequestMapping(value = "/upload")
public class UploadController {

    @Autowired
    private QiNuiUpload qiNuiUpload;



    @RequestMapping(value = "/single")
    public Object upload(MultipartFile file, String key) throws Exception {
        R<SysUpload> r = qiNuiUpload.upload(file.getBytes(), key);
        return r;
    }

}
