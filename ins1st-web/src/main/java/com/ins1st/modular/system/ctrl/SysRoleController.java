package com.ins1st.modular.system.ctrl;

import com.ins1st.api.SysRoleApi;
import com.ins1st.base.R;
import com.ins1st.entity.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-14 11:54
 **/
@Controller
@RequestMapping(value = "/sys/role")
public class SysRoleController {

    private static final String MODEL = "pages/sys/role/";

    @Autowired
    private SysRoleApi sysRoleApi;

    /**
     * 主页
     *
     * @return
     */
    @RequestMapping(value = "/index")
    public String index() {
        return MODEL + "role_index.html";
    }


    /**
     * 分页
     *
     * @param sysRole
     * @return
     */
    @RequestMapping(value = "/page")
    @ResponseBody
    public Object page(SysRole sysRole) {
        return sysRoleApi.page(sysRole);
    }

    /**
     * 添加页
     *
     * @return
     */
    @RequestMapping(value = "/add")
    public String add() {
        return MODEL + "role_add.html";
    }

    /**
     * 编辑页
     *
     * @return
     */
    @RequestMapping(value = "/edit")
    public String edit(Model model, String id) {
        R<SysRole> r = this.sysRoleApi.selectOne(id);
        model.addAttribute("sysRole", r.getData());
        return MODEL + "role_edit.html";
    }

    /**
     * 保存或更新
     *
     * @param sysRole
     * @return
     */
    @RequestMapping(value = "/insertOrUpdate")
    @ResponseBody
    public Object insertOrUpdate(SysRole sysRole) {
        R r = this.sysRoleApi.insertOrUpdate(sysRole);
        return r;
    }


    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String id) {
        R r = this.sysRoleApi.remove(id);
        return r;
    }

    /**
     * 查询树结构
     *
     * @return
     */
    @RequestMapping(value = "/selectZtree")
    @ResponseBody
    public Object selectZtree(String userId) {
        R r = this.sysRoleApi.selectZtree(userId);
        return r;
    }

    /**
     * 保存用户和角色
     * @param userId
     * @param roleIds
     * @return
     */
    @RequestMapping(value = "/saveUserRoles")
    @ResponseBody
    public Object saveUserRoles(String userId, String roleIds) {
        R r = this.sysRoleApi.saveUserRoles(userId, roleIds);
        return r;
    }


}
