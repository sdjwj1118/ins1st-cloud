layui.use(['form', 'jquery', 'ax'], function () {
    let form = layui.form;
    let $ = layui.jquery;
    let $ax = layui.ax;

    form.verify({
        confirmPassword: function (value) {
            if ($("#userPassword").val() != $("#confirmPassword").val()) {
                return "两次密码不一致";
            }
        }
    });

    form.on("submit(add)", function (data) {
        if ($("#userPassword").val() != $("#confirmPassword").val()) {
            admin.error("两次密码不一致");
            return false;
        }
        var ax = new $ax(ctx + "/sys/user/insertOrUpdate", function (result) {
            admin.success(result.message, function () {
                admin.close();
            });
        });
        ax.postForm(data.field);
        ax.start();
        return false;
    });
});