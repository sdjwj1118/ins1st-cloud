package com.ins1st.util;

import java.io.*;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-27 10:48
 **/
public class FileUtil {


    public static void copy(FileInputStream fileInputStream, File out) throws IOException {
        InputStream input = null;
        OutputStream output = null;
        try {
            input = fileInputStream;
            output = new FileOutputStream(out);
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = input.read(buf)) > 0) {
                output.write(buf, 0, bytesRead);
            }
        } finally {
            input.close();
            output.close();
        }
    }
}
